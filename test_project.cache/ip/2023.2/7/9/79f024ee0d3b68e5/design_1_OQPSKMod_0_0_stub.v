// Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
// Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2023.2 (lin64) Build 4029153 Fri Oct 13 20:13:54 MDT 2023
// Date        : Wed Jan 10 23:01:58 2024
// Host        : ilias-HP-Laptop-15-db1xxx running 64-bit Ubuntu 22.04.3 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_OQPSKMod_0_0_stub.v
// Design      : design_1_OQPSKMod_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "OQPSKMod,Vivado 2023.2" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(ap_clk, ap_rst_n, ap_start, ap_done, ap_idle, 
  ap_ready, inStream_TVALID, inStream_TREADY, inStream_TDATA, inStream_TLAST, inStream_TKEEP, 
  inStream_TSTRB, outStream_TVALID, outStream_TREADY, outStream_TDATA, outStream_TLAST, 
  outStream_TKEEP, outStream_TSTRB)
/* synthesis syn_black_box black_box_pad_pin="ap_rst_n,ap_start,ap_done,ap_idle,ap_ready,inStream_TVALID,inStream_TREADY,inStream_TDATA[39:0],inStream_TLAST[0:0],inStream_TKEEP[4:0],inStream_TSTRB[4:0],outStream_TVALID,outStream_TREADY,outStream_TDATA[31:0],outStream_TLAST[0:0],outStream_TKEEP[3:0],outStream_TSTRB[3:0]" */
/* synthesis syn_force_seq_prim="ap_clk" */;
  input ap_clk /* synthesis syn_isclock = 1 */;
  input ap_rst_n;
  input ap_start;
  output ap_done;
  output ap_idle;
  output ap_ready;
  input inStream_TVALID;
  output inStream_TREADY;
  input [39:0]inStream_TDATA;
  input [0:0]inStream_TLAST;
  input [4:0]inStream_TKEEP;
  input [4:0]inStream_TSTRB;
  output outStream_TVALID;
  input outStream_TREADY;
  output [31:0]outStream_TDATA;
  output [0:0]outStream_TLAST;
  output [3:0]outStream_TKEEP;
  output [3:0]outStream_TSTRB;
endmodule
