This is a block design to write data using the Zynq7000 core to the OQPSK modulator and then read the output to confirm it is functional. The software platform and workspace were developed using ```Vitis Unified IDE```, therefore it will most likely not work on ```Vitis Classic```.  To build the block design, the  OQPSK IP should be added to the Vivado project.

**Add OQPSK IP and create block design**
Under ```Flow Navigator``` and ```Project Manager``` on the left of the window, click ```IP Catalog```. Alternatively, click ```Window``` from the top of the page and open ```IP Catalog``` from there. Right-click in that window and then ```Add Repository```. Search for the ```unzipped_rtl_oqpsk``` directory, where this repository was cloned.
After that, open the Tcl console and run the script also found in this repository. The ```test_1.xsa``` is the hardware extracted from this block design.

**Adding workspace**
The sw project can either be opened in the Vitis Unified IDE with ```File```->```Open Workspace``` and open the ```vitis_unified_workspace``` or by creating a new ```Vitis Classic```.
