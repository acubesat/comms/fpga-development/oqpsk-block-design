
#include <stdio.h>
#include "xaxidma.h"
#include <xstatus.h>
#include "platform.h"
#include "xparameters.h"


int main()
{
    init_platform();
    /*
     * Configuration of axi dma
     */
    
    XAxiDma dma_driver;
    XAxiDma_Config * dma_config_pointer = XAxiDma_LookupConfig(XPAR_AXI_DMA_0_BASEADDR);
    if ( !dma_config_pointer ){
        print("Error: Couldn't set pointer to the dma configuration.");
    }
    int status = XAxiDma_CfgInitialize( &dma_driver, dma_config_pointer);
    if (status != XST_SUCCESS){
        print("Initialization failed.");
    }

    uint8_t bitString[] = {0b10101010, 0b11001100, 0b11110000, 0b00001111};
    uint8_t receiveString[4];
    // Pointers for DMA
    uint32_t *TxBufferPtr = (uint32_t *) bitString; 
    uint32_t *RxBufferPtr = (uint32_t *) receiveString;
    
    int TxLength = sizeof(bitString); // Size in bytes    
    int RxLength = sizeof(receiveString);
    // Perform the DMA transfer

    // Transmit
    int Status = XAxiDma_SimpleTransfer(&dma_driver, (UINTPTR)TxBufferPtr, TxLength, XAXIDMA_DMA_TO_DEVICE);
    if (Status != XST_SUCCESS) {
    // Error handling
    }


    Status = XAxiDma_SimpleTransfer(&dma_driver, (UINTPTR)RxBufferPtr, RxLength, XAXIDMA_DEVICE_TO_DMA);
    if (Status != XST_SUCCESS) {
    // Error handling
    }

    for(int i = 0; i < 4; i++){
        xil_printf("%d \n", receiveString[i]);
    }    
    while (XAxiDma_Busy(&dma_driver, XAXIDMA_DEVICE_TO_DMA)) {
        
    }
    // Wait for the transmission to complete
    while (XAxiDma_Busy(&dma_driver, XAXIDMA_DMA_TO_DEVICE)) {
        
    }
    
    cleanup_platform();
    return 0;
}
